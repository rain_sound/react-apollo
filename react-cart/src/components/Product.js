import React from 'react'
import styled from 'styled-components';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';


const Wrapper = styled.div`
    width: 360px;
    height: 106px;
`;


const GET_PRODUCTS = gql`
  {
    products {
      name
      imageProduct
    }
  }
`;
const Product = ({ imgProduct, name, price }) => {
    const { loading, error, data } = useQuery(GET_PRODUCTS);
  
    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;
  
    return (
      <Wrapper>
          { console.log('data', data)}
        <img src={imgProduct} alt="" />
        <h2>{name}</h2>
        <span>{price}</span>
      </Wrapper>
    );
  }

/* const Product = ({ imgProduct, name, price }) => {
    return (
        <Wrapper>
            <img src={imgProduct} alt=""/>
            <h2>{name}</h2>
            <span>{price}</span>
        </Wrapper>
    )
} */

export default Product