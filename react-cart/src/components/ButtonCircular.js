import React from 'react'


const ButtonCircular = ({ handleClick }) => {
    <input onClick={handleClick} type="button" value="Click me" />
}

export default ButtonCircular