
import db from "./models";

export default {
  products: () => db.product.findAll(),
  productsByName: ({ name }) => {
    return db.product.findAll({
      where:{
        name: {
          [db.Sequelize.Op.like]: `%${name}%`
        }
      }
    }).then(res => res)
    .catch(err => err)
  } 
  
};
