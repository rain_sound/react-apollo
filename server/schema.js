import { buildSchema } from 'graphql';

export default buildSchema(`
    type Product {
        id: ID!
        name: String!
        price: Float
        imageProduct: String!
    }

    type Query {
        products: [Product]
        productsByName(name: String!): [Product]
    }
   
`);

