import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';

const client = new ApolloClient();
const query = gql`
  query {
    products {
      name
      imageProduct
    }
  }
`;

const body = document.body;
client.query({ query }).then((results) => {
  console.log('')
  results.data.products.forEach( (product) => renderProduct(body, product) );
});

const renderProduct = (body, product) => {
  console.log('product', product);
  const section = document.createElement('section');
  const domString = `
    <p>
      <strong>Producto: </strong>${product.name}
      <img src=${product.imageProduct}/>
    </p>
    
  `;
  section.innerHTML = domString;
  body.appendChild(section);
};