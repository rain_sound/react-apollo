import db from "./models";
import times from "lodash.times";
import faker from "faker";

export const seedDatabase = () => {
    db.product.bulkCreate(
        times(10, () => ({
          name: faker.internet.userName(),
          price: faker.random.float,
          imageProduct: 'https://picsum.photos/id/72/200/300'
        }))
      );
}
