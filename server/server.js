import express from "express";
import resolvers from "./resolvers";
import db from "./models";
import schema  from "./schema"
import graphqlHTTP from 'express-graphql';
import { seedDatabase } from "./seed"
import cors from 'cors'

var app = express();
var corsOptions = {
  origin: 'http://localhost:3000',
  credentials: true // <-- REQUIRED backend setting
};


app.use(cors(corsOptions))


app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: resolvers,
  graphiql: true,
}));


app.use(express.static("app/public"));


db.sequelize.sync().then(() => {
  //Seed database
  seedDatabase();
  //Init server
  app.listen({ port: 4001 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000`)
  );
});
